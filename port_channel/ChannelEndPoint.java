package port_channel;
import java.io.*;
import java.net.*;
import java.util.concurrent.*; 
import java.util.Random;
import java.io.Serializable;
import java.sql.Timestamp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChannelEndPoint { 
	int nodeId;
	ObjectOutputStream out; 
	BufferedReader br; 
	Socket soc = null; 
	String ip; 
	int port; 
	ConcurrentLinkedQueue<Message> que;
	
	public ChannelEndPoint(int nodeId, String ip, int port) {
		this.nodeId = nodeId; 
		this.ip = ip;
		this.port = port; 
		this.que = new ConcurrentLinkedQueue<Message>(); 
	}

	public void initialize() throws InterruptedException { 
		while (soc == null) {
			try {
				soc = new Socket(ip, port); 
				System.out.println("Socket open. Connection established."); 
				br = new BufferedReader(new InputStreamReader(soc.getInputStream())); 
				out = new ObjectOutputStream(soc.getOutputStream());
			} catch (UnknownHostException e) {
				System.err.println("ERROR: Unknown host."); 
			} catch (IOException e) {
				System.err.println("ERROR: Couldn't get I/O for the connection to server.");
			} 

		}
	} 

	public synchronized void gotMessage(Message message) {
		que.offer(message); 
		notifyAll(); 
	} 
	
	public synchronized Message receive() { 
		while (que.isEmpty()) { 
			try {
				wait(); 
			} catch (InterruptedException ire) {
				ire.printStackTrace();
			}
		} 
		Message msg = que.poll(); 

		return msg; 
	} 
	
	public void send(Message msg) { 
		try { 
			out.writeObject(msg);
		} catch (SocketException se) { 
			System.exit(1);
		} catch (IOException ioe) {
			ioe.printStackTrace(); 
		}
	}

	public synchronized ObjectInputStream getObjectInputStream(){
		ObjectInputStream in = null;
		try{
			in = new ObjectInputStream(soc.getInputStream());
		}
		catch(IOException e){
			e.printStackTrace();
		}
		return in;
	}
	
	public void close() throws IOException { 
		try { 
			send(new MessageString("Shutting down.", 1, 'E'));
			//System.out.println("Shutting down."); 
		} catch (Exception e) { 
			e.printStackTrace();
		} finally {
			br.close();
			out.close();
			soc.close();
		}
	}
	
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException { 
		if (args.length != 3){ 
			System.out.println("usage: java ChannelEndPoint server-ip server-port nodes-ID");
			System.exit(-1);
		} 
		int nodeId = Integer.parseInt(args[2]);
		String ip = args[0]; 
		int port = Integer.parseInt(args[1]); 
		ChannelEndPoint cep = new ChannelEndPoint(nodeId, ip, port); 
		cep.initialize(); 
        Thread.sleep(90000);
        Random rand = new Random();
		ReadThread rt = new ReadThread(cep);
		Thread td = new Thread(rt);	
		td.start();

		for (int j = 0; j < 20; j++) {
			Message msg = new MessageTest(nodeId, Integer.toString(Math.abs(rand.nextInt())), 'T'); 
			System.out.println("sending: " + msg);
			try {
				cep.send(msg);

			} catch (Exception e) {
				e.printStackTrace();
            }


        } 
		td.join();
		cep.close();
	}
	

}


class ReadThread implements Runnable{
	private ChannelEndPoint cep;
	private	ObjectInputStream in;

	ReadThread(ChannelEndPoint cep){
		this.cep = cep;
		this.in = cep.getObjectInputStream();
	}

	public Timestamp parseTimestampFromString (String yourString){
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
			Date parsedDate = dateFormat.parse(yourString);
			Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			return timestamp;
		} catch(Exception e) { //this generic but you can control another types of exception
			// look the origin of excption 
		}
		return null;
	}

	public void run(){
		try{
		MessageTest msg1 = (MessageTest) in.readObject();
		long sum_time = 0;
		long total_messages = 0;
		double f = 0;
	
		while (msg1 != null){
			try{

				if (msg1.pId == cep.nodeId)
					msg1.setTimeStamp();
				
				cep.gotMessage(msg1);
				System.out.println("Client " + msg1.pId + ": " + msg1 + "");
				MessageTest announce = (MessageTest) cep.receive();
				// System.out.println(announce); 

				if(announce == null){
					System.out.println("EXIT.");
					
					
					break;
				}
				
				if (msg1.pId == cep.nodeId){

					total_messages = total_messages + 1;
					long start_time = parseTimestampFromString(msg1.timestamp).getTime();
					long stop_time = parseTimestampFromString(msg1.receiveTimeStamp).getTime();
					sum_time = sum_time + Math.abs(stop_time - start_time);
					f = sum_time / (double)total_messages;

					System.out.println("Average Time: " + Double.toString(f));

				
				}
				
				
				msg1 = (MessageTest) in.readObject();
				if (msg1 == null){
					break;
				}


			} catch(ClassNotFoundException e){
				e.printStackTrace();
			} catch (EOFException e){
				cep.close();
				System.exit(0);
			}catch (SocketException e){
				cep.close();
				System.exit(0);
			}

			

		} 

		


	}catch(Exception e){
		e.printStackTrace();
	}
}


}
