package port_channel;


import java.util.*;
import java.util.concurrent.*;

import DistSemaphore.Initiator;

import java.io.*;
import java.net.*; 

public class ChannelPort implements Runnable {
	int portNum; 
	int nSize; 
	ObjectOutputStream[] outs; 
	Listner[] listners; 
	ConcurrentLinkedQueue<Message> que;
	int connectionCount;
	ServerSocket serverSocket = null; 
	Socket clientSocket = null; // not part of communication 
	Initiator initiator = null;
	
	public ChannelPort(int portNum, int networkSize) {
		this.portNum = portNum; 
		this.nSize = networkSize; 
		this.outs = new ObjectOutputStream[nSize]; 
		this.listners = new Listner[nSize]; 
		this.que = new ConcurrentLinkedQueue<Message>(); 
		this.connectionCount = 0;
	} 
	
	public ChannelPort(int portNum, int networkSize, Initiator init){
		this.portNum = portNum; 
		this.nSize = networkSize; 
		this.outs = new ObjectOutputStream[nSize]; 
		this.listners = new Listner[nSize]; 
		this.que = new ConcurrentLinkedQueue<Message>(); 
		this.connectionCount = 0;
		this.initiator = init;

	}
	public void initialize() {
		try {
			this.serverSocket = new ServerSocket(portNum); 
		} catch (IOException ioe) {  
			ioe.printStackTrace();
		} 
		for (int j = 0; j < nSize; j++) { 
			addListner(j);
		}
		
		if (this.initiator != null){
			this.initiator.setBoolean(true);
		}

		System.out.println("Connections are all established.");
	}

	public void addListner(int listnerNumber){
		try {
			this.clientSocket = serverSocket.accept(); // not part of communication 
			outs[listnerNumber] = new ObjectOutputStream(clientSocket.getOutputStream()); 
			ObjectInputStream in = new ObjectInputStream(clientSocket.getInputStream()); 
			listners[listnerNumber] = new Listner(listnerNumber, in, this);  
			if (this.initiator != null){
				getIPandNodeID(in);
			}
		} catch (IOException ioe) { 
			System.err.println("Connection failed for listner: " + listnerNumber);
			ioe.printStackTrace(); 
			System.exit(-1);
		} 
	}
	//IP and Port seperated by :
	public void getIPandNodeID(ObjectInputStream in){
		MessageString msg = (MessageString) in.readObject();
		String str = msg.message;
		String[] str1 = str.split(":");

		this.initiator.ipList.put(msg.pId, new Nodeinfo(InetAddress.getByName(str1[0]), msg.pId, Integer.parseint(str1[1])));
	}

	public void run() { 
		initialize(); 
		for (int j = 0; j < nSize; j++) {
			listners[j].start(); 
		}
	}
	
	synchronized void gotMessage(Message message) {
		que.offer(message); 
		notifyAll(); 
	} 
	
	public synchronized Message receive() { 
		while (que.isEmpty()) { 
			try {
				wait(); 
			} catch (InterruptedException ire) {
				ire.printStackTrace();
			}
		} 
		Message msg = que.poll(); 

		return msg; 
	} 
	
	public synchronized void broadcast(Message message) { 
        System.out.println("Broadcasting "+ message.toString());
		for (int j = 0; j < outs.length; j++) {
			try{
				if(outs[j] != null){ //outs[j] is set to null below if it was closed on the other end.
					outs[j].writeObject(message);
					outs[j].flush();
				}
			}catch(SocketException e){
				outs[j] = null; //Set to null if closed on the other end.
			}
			catch(IOException e){
				e.printStackTrace();
			}

		}
	}
	
	public synchronized void close(){
		for (int j = 0; j < outs.length; j++) { 
			listners[j].close();
		}
	}

	public int getPortNum() {
		return portNum;
	}

	public void setPortNum(int portNum) {
		this.portNum = portNum;
	}

	public int getnSize() {
		return nSize;
	}

	public ConcurrentLinkedQueue<Message> getQue() {
		return que;
	}

	public static void main(String[] args) throws IOException, InterruptedException{ 
		if (args.length != 2){
			System.out.println("usage: java ChannelPort port-number number-of-nodes");
			System.exit(-1);
		} 
		int portNum = Integer.parseInt(args[0]); 
		int numNode = Integer.parseInt(args[1]); 
		ChannelPort cp = new ChannelPort(portNum, numNode); 
		new Thread(cp).start(); 
		Thread.sleep(90000); 
		System.out.println("Shutdown"); 
		Iterator<Message> ite = cp.getQue().iterator(); 
		while (ite.hasNext()) {
			System.out.println(ite.next()); 
		}
	}
}

class Listner extends Thread { 
	int pId; 
	ObjectInputStream in; 
	ChannelPort cPort; 
	boolean connected = true;
	final int ERR_THRESHOLD = 50; 
	
	public Listner(int id, ObjectInputStream in, ChannelPort cPort) { 
		this.pId = id; 
		this.in = in;  
		this.cPort = cPort; 
	}

	public void close(){
		this.connected = false;
	}
	
	public void run() { 
		MessageTest msg; 
		int errCnt = 0;
		
		while(connected == true) { 
			try {

				msg = (MessageTest)in.readObject(); 
				System.out.println("Client " + pId + ": " + msg + "");
				

				cPort.gotMessage(msg);
				cPort.receive();
				cPort.broadcast(msg);
			} catch (ClassNotFoundException cnfe) { 
				cnfe.printStackTrace(); 
			} catch (SocketException se) { 
				//System.err.println(se);
				errCnt++; 
				if (errCnt > ERR_THRESHOLD){ //If error count exceeds the threshold, exit the loop.
					connected = false;
				}
			} catch (EOFException ioe) { //If End of File (or stream), exit the loop.
				connected = false; 
				System.err.println("Connection " + pId + " terminated");
			} 
			catch (IOException ioe) {
				ioe.printStackTrace(); 
			}
		}
	}
}
