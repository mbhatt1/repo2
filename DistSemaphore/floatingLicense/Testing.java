package floatingLicense;

import java.io.IOException;

import java.lang.InterruptedException;
import DistSemaphore.DistSemaphore;



public class Testing{
    DistSemaphore sem;
    int accBalance;
    
    public Testing(int amt, DistSemaphore sem) {
        this.sem = sem;
        this.accBalance = amt;
    } 
    public void donate(int amt) throws IOException{
        System.out.println("Getting P in Donate");
        sem.P();
        accBalance = accBalance +  amt;

        System.out.println(Integer.toString(this.accBalance));
        try {Thread.sleep(amt); } catch (Exception e) {e.printStackTrace();}
        System.out.println("Releasing Lock");
        sem.V();
    }
    
    public void spend(int amt) throws IOException {
        System.out.println("Getting P in Spend");
        sem.P();
        accBalance = accBalance - amt;
        System.out.println(Integer.toString(this.accBalance));
        try {Thread.sleep(amt*10); } catch (Exception e) {e.printStackTrace();}
        System.out.println("Releasing Lock");
        sem.V();
    }




    public static void main(String[] args) throws IOException, InterruptedException, IOException{
        if (args.length != 2){
            System.out.println("java Tester port IpInitiator");
        }

        try{
            Thread.sleep(12000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }

        int port = Integer.parseInt(args[0]);
        DistSemaphore sem  = null;
        try{
            sem = new DistSemaphore("bleh", args[1], port);
        } catch (IOException e){
            e.printStackTrace();
        }
        Testing tester = new Testing(1000, sem);
        System.out.println(Integer.toString(tester.accBalance));
        try{
            tester.donate(100);
            
            tester.spend(10);


        } catch (IOException e ){
            e.printStackTrace();
        }

        while(true){
            continue;
        }

    } 
}