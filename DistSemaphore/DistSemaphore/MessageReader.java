// package DistSemaphore;


// import java.util.ArrayList;
// import java.util.Collections;

// import java.util.Arrays;
// import java.io.IOException;
// import java.io.DataOutputStream;
// import java.io.DataInputStream;

// class MessageReader implements Runnable
// {
//     int readerIndex;
//     int helperIndex;
//     DisSemHelper diss;
//     Connection coordinatorConnection = null;
//     public MessageReader(int readerIndex, int helperIndex, DisSemHelper that, Connection conn) {
//         super();
//         this.readerIndex = readerIndex;
//         this.helperIndex = helperIndex;
//         this.diss = that;
//         this.coordinatorConnection = new Connection;
//     }
//     @Override
//     public void run() {
//         DataInputStream dis;
//         Message recievedMessage;
//         while(true)
//         {
//             //Make requisite Connections

//             if(readerIndex==helperIndex)
//             {
//                 if(diss.userConnected==false)
//                 {
//                     DataIO dio=coordinatorConnection.acceptConnect();
                
//                     DataInputStream dai=dio.getDis();
//                     DataOutputStream dao=dio.getDos();
//                     diss.helperInputStreams[helperIndex]=dai;
//                     diss.helperOutputStream[helperIndex]=dao;
//                     try {
//                         int k=dai.readInt();
//                         if(k!=-1) throw new Error("Can only connect to user programs");
//                         System.out.printf("%d program connected\n",helperIndex);
//                     } catch (IOException e) {
//                         System.out.println("Communicating with user program");
//                         e.printStackTrace();
//                     }
//                 }
//                 try {
//                     diss.helperOutputStream[helperIndex].writeInt(helperIndex);
//                 } catch (IOException e) {
//                     System.out.println("Communicating with user program");
//                     e.printStackTrace();
//                 }
//             }

//             // Start Responding to Messages
//             //// System.out.printf("%d,%d message %s sent by %d at %d\n",helperIndex,readerIndex,skind,sender,ts);
//             dis=diss.helperInputStreams[readerIndex];
//             while(true)
//             {
//                 //read a message
//                 try{
//                     int sender=dis.readInt();
//                     String skind=dis.readUTF();
//                     int ts=dis.readInt();
//                     DisSemHelper.Kind kind= DisSemHelper.Kind.valueOf(skind);
                    
//                     recievedMessage=new Message(sender, kind, ts);
//                     synchronized (diss.semaphoreQueueLock) 
//                     {
//                         System.out.println("Current value on "+ Integer.toString(this.helperIndex) + " is " + Integer.toString(diss.lc));
//                         diss.lc=Math.max(diss.lc, ts+1);
//                         diss.lc++;
//                         if(kind== DisSemHelper.Kind.reqP)
//                         {
//                             if(sender!=helperIndex) throw new Error("other's reqP");
//                             Message message=new Message(helperIndex, DisSemHelper.Kind.POP, diss.lc);
//                             System.out.println("Broadcasting Message: " + message.toString());
//                             diss.broadcast(message);
//                             diss.lc++;
//                             diss.insert(message);
//                             //since own pop are not recieved
//                             diss.broadcast(new Message(helperIndex,DisSemHelper.Kind.ACK, diss.lc));
//                             //own vop recieved and acked by self
//                             diss.lc++;
//                         }
//                         else if(kind==DisSemHelper.Kind.reqV)
//                         {
//                             if(sender!=helperIndex) throw new Error("other's reqV");
//                             Message message=new Message(helperIndex, DisSemHelper.Kind.VOP, diss.lc);
//                             System.out.println("Broadcasting Message: " + message.toString());
//                             diss.broadcast(message);
//                             diss.lc++;
//                             diss.insert(message); //since own vop are not recieved
//                             diss.broadcast(new Message(helperIndex, DisSemHelper.Kind.ACK, diss.lc));
//                             diss.lc++;
//                         }
//                         else if(kind == DisSemHelper.Kind.POP|| kind== DisSemHelper.Kind.VOP)
//                         {
//                             diss.insert(recievedMessage);
//                             System.out.println("Received Message to broadcast: " + recievedMessage.toString());
//                             diss.broadcast(new Message(helperIndex, DisSemHelper.Kind.ACK, diss.lc));
//                             //note own acks are never sent
//                             diss.lc++;
//                         }
//                         else if(kind==DisSemHelper.Kind.ACK)
//                         {
//                             diss.acknowledgedTimeStamps[sender]=Math.max(diss.acknowledgedTimeStamps[sender], ts);
//                             diss.acknowledgedTimeStamps[helperIndex] = diss.lc;
                            
//                             int minACK=Collections.min(Arrays.asList(diss.acknowledgedTimeStamps));
                            
//                             ArrayList<Message> m=new ArrayList<Message>(diss.mq);
                            
//                             for(Message mess:m)
//                             {
//                                 if(mess.kind== DisSemHelper.Kind.VOP)
//                                 {
//                                     if(mess.timestamp<=minACK)
//                                     {
//                                         System.out.printf("%d %d acknowledged  %s\n",helperIndex,readerIndex,mess.toString());
//                                         diss.mq.remove(mess);
//                                         System.out.println("Removing Message " + mess.toString());
//                                         diss.s++;
//                                     }
//                                     else break;//other messages have to be older than this
//                                 }
                                
//                             }

//                             m=new ArrayList<Message>(diss.mq);
//                             for(Message mess:m)
//                             {
//                                 if(diss.s<=0) {
//                                     System.out.println("Out of concurrent semaphores");
//                                     break;
//                                 } // Semaphore thing

//                                 if(mess.kind == DisSemHelper.Kind.POP)
//                                 {
//                                     if(mess.timestamp<=minACK)
//                                     {
//                                         System.out.printf("%d %d acknowledged  %s\n",helperIndex,readerIndex,mess.toString());
//                                         System.out.println("Removing Message " + mess.toString());
//                                         diss.mq.remove(mess);
                                         
//                                         diss.s--;
//                                         if(mess.sender==helperIndex)
//                                         {
//                                             diss.helperOutputStream[helperIndex].writeInt(helperIndex);
//                                             diss.helperOutputStream[helperIndex].writeUTF(DisSemHelper.Kind.GO.toString());
//                                             diss.helperOutputStream[helperIndex].writeInt(diss.lc);
//                                             diss.lc++;
//                                         }
//                                     }
                                        
//                                 }
                                
//                             }
                                    
//                         }
                        
//                     }
                    
//                 }
                
//                 catch(Throwable e)
//                 {
//                     if(readerIndex==helperIndex){
//                         System.out.printf("%d Program disconnected",helperIndex);
//                     }
//                     else{
                    
//                         System.out.println(helperIndex+" disconnected");
//                     }
//                     e.printStackTrace();
//                     return;
//                     //System.exit(1);
//                 }
//             }
            
            
//         }
        
//     }
    
// }