package matrix_multiply_square;
import java.io.*; 
import java.net.*; 
import java.util.List;
import java.util.Collections;

class Connection {

	String name; 
	int port; 
	ServerSocket servsoc; 
	
	public static String localHostAddress() {
		try {
			List<NetworkInterface> nis = Collections.list(NetworkInterface.getNetworkInterfaces()); 
			for (NetworkInterface ni : nis) {
				if (!ni.isLoopback() && ni.isUp() && ni.getHardwareAddress() != null) {
					for (InterfaceAddress ia : ni.getInterfaceAddresses()) {
						if (ia.getBroadcast() != null) { 
							return ia.getAddress().getHostAddress();
						}
					}
				}
			}
		} catch (SocketException e) {
			e.printStackTrace(); 
		} 
		return null; 
	} 

	public Connection(int portNum) { 
		this.port = portNum; 
		try {
			servsoc = new ServerSocket(port); 
		} catch (IOException ioe) {
			System.out.println("Could not listen on port: " + port + ", " + ioe);  
			System.exit(1); 
		}
	} 
	
	DataIO acceptConnect() {
		DataIO dio = null;  
		while (true) {
			try {
				Socket sc = servsoc.accept(); 
				DataInputStream dis = new DataInputStream(sc.getInputStream()); 
				DataOutputStream dos = new DataOutputStream(sc.getOutputStream()); 
				dio = new DataIO(dis, dos); 
				break; 
			} catch (IOException ioe) {
				System.out.println("Fail in connecting to worker node.");
				try {Thread.sleep(2000); } catch (Exception e) {System.out.println("Try again."); }
			}
		} 
		return dio;
	} 
	
	DataInputStream accept2read() {
		DataInputStream dis = null;  
		Socket sc =  null;
		while (true) {
			try {
				sc = servsoc.accept(); 
				dis = new DataInputStream(sc.getInputStream());
				break; 
			} catch (IOException ioe) {
				System.out.println("Fail in connecting to worker node.");
				try {Thread.sleep(2000); } catch (Exception e) {System.out.println("Try again."); }
			}
		}
		//For debugging. Print the incoming address.
		InetSocketAddress isa = (InetSocketAddress) sc.getRemoteSocketAddress();
		InetAddress addr = isa.getAddress();
		System.out.println(addr.toString() + ":" + sc.getPort());
		return dis;
	} 
	
	DataOutputStream accept2write() {
		DataOutputStream dos = null;  
		while (true) {
			try {
				Socket sc = servsoc.accept(); 
				dos = new DataOutputStream(sc.getOutputStream()); 
				break; 
			} catch (IOException ioe) {
				System.out.println("Fail in connecting to worker node.");
				try {Thread.sleep(2000); } catch (Exception e) {System.out.println("Try again."); }
			}
		}
		return dos;
	} 

	DataIO connectIO(String ip, int port) {
		DataIO dio = null;
		while (true) {
			try {
				Socket sc = new Socket(ip, port);
				DataInputStream dis = new DataInputStream(sc.getInputStream());
				DataOutputStream dos = new DataOutputStream(sc.getOutputStream()); 
				dio = new DataIO(dis, dos); 
				break; 
			} catch (IOException ioe) {
				System.out.println("Fail in connecting to ip=" + ip + ", port="	+ port);
				try {Thread.sleep(200);	} catch (Exception e) {System.out.println("Try again."); }
			}
		}
		return dio;
	}
	
	DataOutputStream connect2write(String ip, int port) {
		DataOutputStream dos = null;
		while (true) {
			try {
				Socket sc = new Socket(ip, port);
				dos = new DataOutputStream(sc.getOutputStream()); 
				break; 
			} catch (IOException ioe) {
				System.out.println("Fail in connecting to ip=" + ip + ", port="	+ port);
				try {Thread.sleep(200);	} catch (Exception e) {System.out.println("Try again."); }
			}
		}
		return dos;
	}
	
	DataInputStream connect2read(String ip, int port) {
		DataInputStream dis = null;
		while (true) {
			try {
				Socket sc = new Socket(ip, port);
				dis = new DataInputStream(sc.getInputStream()); 
				break; 
			} catch (IOException ioe) {
				System.out.println("Fail in connecting to ip=" + ip + ", port="	+ port);
				try {Thread.sleep(2000); } catch (Exception e) {System.out.println("Try again."); }
			}
		}
		return dis;
	} 
}