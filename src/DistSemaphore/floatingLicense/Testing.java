package floatingLicense;

import java.io.IOException;

import DistSemaphore.DistSemaphore;



public class Testing{
    DistSemaphore sem;
    int accBalance;
    
    public Testing(int amt, DistSemaphore sem) {
        this.sem = sem;
        accBalance = amt;
    } 
    public void donate(int amt) throws IOException{
        sem.P();
        accBalance =+ amt;
        try {Thread.sleep(amt); } catch (Exception e) {e.printStackTrace();}
        sem.V();
    }
    
    public void spend(int amt) throws IOException {
        sem.P();
        accBalance =- amt;
        try {Thread.sleep(amt*10); } catch (Exception e) {e.printStackTrace();}
        sem.V();
    }


    public static void main(String[] args) throws IOException{
        if (args.length != 2){
            System.out.println("java Tester port IpInitiator");
        }

        int port = Integer.parseInt(args[0]);
        DistSemaphore sem  = null;
        try{
            sem = new DistSemaphore("bleh", args[1], port);
        } catch (IOException e){
            e.printStackTrace();
        }
        Testing tester = new Testing(1000, sem);
        tester.donate(100);
        tester.spend(10);
    } 
}