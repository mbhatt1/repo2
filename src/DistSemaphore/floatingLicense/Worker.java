package floatingLicense;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import DistSemaphore.*;


public class Worker {
	public static void main(String []args)
	{
		if (args.length != 5) {
				System.out.println("usage: java Worker ConfigIP PORT nodeID users myport");
			
				System.exit(1);
        }
        
        //Distributed Semaphore Variable
        DistSemaphore dissem = null;
        int nodeID = Integer.parseInt(args[2]);
        
        System.out.println("Connecting to initiator");


		while (true) {

			try {
				if (dissem == null)
                    dissem = new DistSemaphore("bla", args[0], Integer.parseInt(args[1]));
                    

                BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
                
                //Local variable to verify status of the semaphore
                
                int lockObtained=0;

                String license = "$$YAY! LICENSE ALL DAY$$";
				while(true)
				{
                        //Lock Prompt
                        System.out.printf("1. Request License Access \n2. I'm done using the License\n");
                        if (lockObtained == 0)
                            System.out.println("I don't have the lock right now");
                        else
                            System.out.println("I hold the lock right now. Others should be spinning");
                    
                    
                    int i;
					try{
                        i=Integer.parseInt(reader.readLine());
                        if(i!=1 && i!=2) 
                            throw new Error("Invalid input");
					}catch(Throwable e)
					{
						System.out.println("Invalid input, try again");
						continue;
                    }
                    

					if(i==2 && lockObtained<=0)
					{
						System.out.println("You don't have the lock");
						continue;
                    }
                    

					if(i==2)
					{
                            System.out.println("Attempting return");
                            dissem.V();
                            lockObtained--;
                            System.out.println("Done return");
                    }
                    

					else if(i==1)
					{
						System.out.println("Attempting to request");
						dissem.P();
                        lockObtained++;
                        
                        System.out.println("Received Lock. This is my license: " + license);

                        System.out.println(dissem.lc);

                        System.out.println(dissem.ts);
						
					}
				}
			} catch (IOException e) {

				e.printStackTrace();
			} catch (Throwable e) {
				System.out.println("Failed to initialize, will attempt again");

			}

        }

		
	}
	
}