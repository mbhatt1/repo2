package DistSemaphore;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.PriorityBlockingQueue;



public class DisSemHelper implements Runnable {
	protected enum Kind{reqP,reqV,VOP,POP,ACK,GO};


	
	protected PriorityBlockingQueue<Message> mq;

	protected volatile int lc=0;

	protected volatile int s=2;
	
	
	String configuratorIP;
	int configuratorPort;
	int helperIndex;
	int helperNetworkPort;
	Connection coordinatorConnection;
	Connection [] helperNetworkConnection;
	DataOutputStream coordinatorWrite;
	DataInputStream coordinatorRead;
	String [] helperAddresses;
	int [] helperPort;
	private int helperCount=0;
	DataInputStream [] helperInputStreams;
	DataOutputStream [] helperOutputStream;
	Thread [] communicationThreads;
	Integer [] acknowledgedTimeStamps;
	Object semaphoreQueueLock=new Object();
	
	boolean userConnected;


	public DisSemHelper(String configuratorIP, int configuratorPort, int helperIndex, int helperNetworkPort, int noConcurrentUsers) {
		super();
		this.s = noConcurrentUsers;
		this.configuratorIP = configuratorIP;
		this.configuratorPort = configuratorPort;
		this.helperIndex = helperIndex;
		this.helperNetworkPort=helperNetworkPort ;
	}



	
	public class MessageComparator implements Comparator<Message>
	{

		@Override
		public int compare(Message o1, Message o2) {
			// TODO Auto-generated method stub
			int result=Integer.compare(o1.timestamp, o2.timestamp);
			if(result==0) result=Integer.compare(o1.sender, o2.sender);
			if(result==0) throw new Error("same sender with same timestamp");
			return result;
		}
		
	};


	void makeConnections() throws IOException
	{
		
		for(int i=0;i<helperCount;i++)
		{
			DataInputStream dis;
			DataOutputStream dos;
			int k;
			if(i<helperIndex)
			{
				System.out.printf("Helper %d helper %d at %s:%d\n",helperIndex,i,helperAddresses[i], helperPort[i]);
				DataIO dio=coordinatorConnection.connectIO(helperAddresses[i], helperPort[i]);
				dis=dio.getDis();
				dos=dio.getDos();
				dos.writeInt(helperIndex);
				k=i;
				System.out.printf("Helper %d helper %d at %s:%d\n",helperIndex,i,helperAddresses[i], helperPort[i]);
			}
			else if(i>helperIndex){
				DataIO dio=coordinatorConnection.acceptConnect();
				dis=dio.getDis();
				dos=dio.getDos();
				k=dis.readInt();
				if(k!=-1)
					System.out.printf("Helper %d received connection %d at %s:%d\n",helperIndex,k,helperAddresses[k], helperPort[k]);
			}

			else 
				continue;
			
			if(k==-1) //my user connected
			{
				System.out.printf("%d User program connected\n",helperIndex);
				i--;//will need to connect to one more helper
				userConnected=true;
				k=helperIndex;
			}
			helperInputStreams[k]=dis;
			helperOutputStream[k]=dos;
			
			
		}
		mq=new PriorityBlockingQueue<Message>(helperCount,new MessageComparator());

	}
	protected void broadcast(Message message) throws IOException
	{
		if(message.kind!=Kind.VOP&&message.kind!=Kind.ACK&&message.kind!=Kind.POP)
			throw new Error("All connections already made");
		for(int i=0;i<helperCount;i++)
		{
			if(i==helperIndex) continue; //don't broad cast to yourself
			helperOutputStream[i].writeInt(message.sender);
			helperOutputStream[i].writeUTF(message.kind.toString());
			helperOutputStream[i].writeInt(message.timestamp);
		}
	}

	protected void insert(Message message)
	{
		//insert at apt position-oldest message first
		synchronized (semaphoreQueueLock) {
			mq.add(message);
			System.out.println("Queue:");
			
			for (Message msg : mq){
				System.out.println(msg.toString());
			}
		}
		
	}


	@Override
	public void run() {
		configurate();
		System.out.printf("%d helper has been configured\n",helperIndex);
		try {
			makeConnections();
			System.out.printf("%d made helper connections\n",helperIndex);
		
			synchronized (semaphoreQueueLock) {
			
				//start listening for connection to program
				for(int i=0;i<helperCount;i++)
				{
					communicationThreads[i]=new Thread(new MessageReader(i, helperIndex, this, coordinatorConnection),"Message reader "+helperIndex +","+i);
					communicationThreads[i].start();
					
				}
				System.out.printf("%d communication threads\n",helperIndex);
			}
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	void configurate()
	{
		this.configurate(configuratorIP, configuratorPort);
	}

	void configurate(String coorIP, int coorPort) {
		try {
			System.out.printf("[%d]Configuring Helper \n",helperIndex);
			coordinatorConnection=new Connection(helperNetworkPort);
			DataIO dio = coordinatorConnection.connectIO(coorIP, coorPort);
			coordinatorWrite=dio.getDos();
			coordinatorRead=dio.getDis();
			coordinatorWrite.writeInt(helperIndex);
			coordinatorWrite.writeUTF(coorIP);
			coordinatorWrite.writeInt(helperNetworkPort);
			System.out.printf("[%d]Reading\n",helperIndex);
			helperCount=coordinatorRead.readInt();
			helperAddresses=new String[helperCount];
			helperPort=new int[helperCount];
			helperInputStreams=new DataInputStream[helperCount];
			helperOutputStream=new DataOutputStream[helperCount];
			communicationThreads=new Thread[helperCount];
			acknowledgedTimeStamps=new Integer[helperCount];

			for(int i=0;i<helperCount;i++)
			{
				acknowledgedTimeStamps[i]=0;
				helperAddresses[i]=coordinatorRead.readUTF();
				helperPort[i]=coordinatorRead.readInt();
			}

		} catch (IOException ioe) {
			ioe.printStackTrace();
			throw new Error("Error");
		}
	}

	
	

}