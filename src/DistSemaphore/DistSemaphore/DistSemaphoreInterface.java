package DistSemaphore;

import java.io.IOException;

public interface DistSemaphoreInterface{
    public void P() throws IOException;
    public void V() throws IOException;
}