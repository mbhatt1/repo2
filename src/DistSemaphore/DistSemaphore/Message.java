package DistSemaphore;

// import DistSemaphore.Kind;

public class Message{


    public Message(int sender, DisSemHelper.Kind kind, int timestamp) {

        super();
        this.sender = sender;
        this.kind = kind;
        this.timestamp = timestamp;
    }
    int sender;
    DisSemHelper.Kind kind;
    int timestamp;
    @Override
    public String toString() {
        return String.format("(%s by %d at %d)",kind,sender,timestamp);
    }
    
}