package portecho_nio;

import port_channel.*;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.net.Socket;
import java.util.Iterator;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.*; 
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.Charset;

public class ChannelEndPoint implements Runnable{

    //The host:port combination to monitor
    private InetAddress hostAddress;
    private int port;
    //The channel on which to accept connections
    private SocketChannel socketChannel;
    //the selector to monitor;
    private Selector selector;
    //the buffer into which to read data as it's available
    private ByteBuffer readBuffer;
    //A queue where the messages are stored
    private ConcurrentLinkedQueue<ByteBuffer> que;
    //Charset encoder
    private CharsetEncoder encoder;
    //Charset decoder
    private CharsetDecoder decoder;
    private int nodeId;
    private boolean quit;

    public ChannelEndPoint(int nodeId, String hostAddress, int port){

        try{
            this.hostAddress = InetAddress.getByName(hostAddress);
        } catch(UnknownHostException e){
            System.err.println("Unknown Host");
            System.exit(-1);
        }
        
        this.port = port;
        this.readBuffer = ByteBuffer.allocate(8192);
        this.que = new ConcurrentLinkedQueue<ByteBuffer>();
        this.selector = this.initSelector();
        this.socketChannel = initiateConnection();
        this.encoder = Charset.forName("UTF-8").newEncoder();
        this.decoder = Charset.forName("UTF-8").newDecoder();
        this.nodeId = nodeId;
        this.quit = false;

    }

    protected SocketChannel initiateConnection(){

        SocketChannel socketChannel = null;

        try{
            // Create a non-blocking socket channel
            socketChannel = SocketChannel.open();
            //socketChannel.configureBlocking(false);
        
            // Kick off connection establishment
            socketChannel.connect(new InetSocketAddress(this.hostAddress, this.port));
        } catch(ConnectException e){
            System.err.println("Could not connect to host " + hostAddress.toString());
            System.exit(-1);
        } catch(IOException e){
            e.printStackTrace();
        }
		
		return socketChannel;
	}

	protected Selector initSelector(){
        // Create a new selector
        Selector s = null;
        try{
            s = SelectorProvider.provider().openSelector();
        } catch(IOException e){
            e.printStackTrace();
        }
		return s;
    }
    
    protected void finishConnection(SelectionKey key) {
      
        // Finish the connection. Throws an IOException if the connection fails
        try {
          this.socketChannel.finishConnect();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }

	public synchronized void gotMessage(ByteBuffer message) {
		que.offer(message); 
		notifyAll(); 
	} 
	
	public synchronized ByteBuffer getMessage() { 
		while (que.isEmpty()) { 
			try {
				wait(); 
			} catch (InterruptedException ire) {
				ire.printStackTrace();
			}
		} 
		ByteBuffer msg = que.poll(); 

		return msg; 
    } 
    
	public void send(Message message){
        try{
            //System.out.println("sending: " + message.toString());
            //Convert the string to a byte array, using the UTF-8 Charset
            //byte[] bytes = message.getBytes(Charset.forName("UTF-8"));
            //Convert the byte array to a ByteBuffer and write it
            this.socketChannel.write(encoder.encode(CharBuffer.wrap(message.toString().toCharArray())));
        }
        catch(IOException e){
            System.err.println("There is no connection.");
        }
    }
    
    protected Message receive() throws IOException{
        //Clear the read buffer for new data
        this.readBuffer.clear();

        //Attempt to read the channel
        int numRead;
        Message m = null;
        try{
            numRead = this.socketChannel.read(this.readBuffer);

            //flip the buffer to be read from
            this.readBuffer.flip();

            //TODO: This is going to have to involve some parsing logic with another method call.
            gotMessage(this.readBuffer);

            //Convert the ByteBuffer into a CharBuffer, then into a string.
            CharBuffer b = decoder.decode(getMessage());

            m = Message.parse(b.toString());

        } catch(IOException e){
            //The remote closed the session. Cancel the key and close the channel.
            this.socketChannel.close();
        }
        return m;
    }
	
	public void close(){ 
		try { 
			send(new MessageString("Shutting down.", this.nodeId, 'E'));
		} catch (Exception e) { 
			e.printStackTrace();
		} finally {
			//call .close methods here.
		}
    }

    public boolean quit(){
        return this.quit;
    }

    /**
     * Busy waits until a channel registered with the selector is in an acceptable or readable state.
     */
    public void run(){
        Message msg;
        while(this.quit == false){
            try{
                msg = receive();
                if(msg.messageType() == 'E'){
                    System.out.println("----------------NOTICE: Connection termianted by host. Press enter to exit.---------------------");
                    this.quit = true;
                }
                System.out.println(msg.toString());
                System.out.print(">>>");
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args){
		if (args.length != 3){
			System.out.println("usage: java ChannelEndPoint server-ip server-port nodes-ID");
			System.exit(-1);
		} 
		int portNum = Integer.parseInt(args[1]); 
        int nodeId = Integer.parseInt(args[2]); 
        String address = args[0];

        nio_portchannel.ChannelEndPoint nc = new nio_portchannel.ChannelEndPoint(nodeId, address, portNum);

		for (int j = 0; j < 5; j++) {
            String message = nodeId + "-message-" + j;
			try {
                nc.send(new MessageString(message, nodeId, 'T'));
                Message m = nc.receive();
                System.out.println(m.toString()); 
                
			} catch (Exception e) {
				e.printStackTrace();
			}
		} 
		
		//System.out.println(announce.getMessage()); 
		nc.close();
    }
}