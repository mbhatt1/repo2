package port_channel_nio;



import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.*; 
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.Charset;
import java.util.*;

public class ChannelPort implements Runnable{
    private InetAddress hostAddress;
    private int port;

    // The channel on which we'll accept connections
    private ServerSocketChannel serverChannel;

    // The selector we'll be monitoring
    public Selector selector;

    // The buffer into which we'll read data when it's available
    private ByteBuffer readBuffer = ByteBuffer.allocate(8192);

    private EchoWorker worker;

    // A list of PendingChange instances
    private final List<ChangeRequest> pendingChanges = new LinkedList<ChangeRequest>();

    private HashSet<SocketChannel> scl = new HashSet<SocketChannel>();

    // Maps a SocketChannel to a list of ByteBuffer instances
    private final Map<SocketChannel, List<ByteBuffer>> pendingData = new HashMap<SocketChannel, List<ByteBuffer>>();

    public ChannelPort(InetAddress hostAddress, int port, EchoWorker worker) throws IOException {
        this.hostAddress = hostAddress;
        this.port = port;
        this.worker = worker;
        initSelector();
        this.worker.setSelector(this.scl);
    }

    public void send(SocketChannel socketChannel, byte[] data) {
        synchronized (pendingChanges) {
            // Indicate we want the interest ops set changed
            pendingChanges.add(new ChangeRequest(socketChannel, ChangeRequest.CHANGEOPS, SelectionKey.OP_WRITE));

            // And queue the data we want written
            synchronized (pendingData) {
                List<ByteBuffer> queue = pendingData.get(socketChannel);
                if (queue == null) {
                    queue = new ArrayList<ByteBuffer>();
                    pendingData.put(socketChannel, queue);
                }
                queue.add(ByteBuffer.wrap(data));
            }
        }

        // Finally, wake up our selecting thread so it can make the required changes
        selector.wakeup();
    }

    public void run() {
        while (true) {
            try {
                // Process any pending changes
                synchronized (pendingChanges) {
                    for (ChangeRequest pendingChange : pendingChanges) {    // TODO same in NioClient
                        switch (pendingChange.type) {
                            case ChangeRequest.CHANGEOPS:
                                SelectionKey key = pendingChange.socket.keyFor(selector);
                                key.interestOps(pendingChange.ops);
                        }
                    }
                    pendingChanges.clear();
                }

                // Wait for an event one of the registered channels
                selector.select();

                // Iterate over the set of keys for which events are available
                Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
                while (selectedKeys.hasNext()) {
                    SelectionKey key = selectedKeys.next();
                    selectedKeys.remove();

                    if (!key.isValid()) {
                        continue;
                    }

                    // Check what event is available and deal with it
                    if (key.isAcceptable()) {
                        accept(key);
                    } else if (key.isReadable()) {
                        read(key);
                    } else if (key.isWritable()) {
                        write(key);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void accept(SelectionKey key) throws IOException {
        // For an accept to be pending the channel must be a server socket channel.
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();

        // Accept the connection and make it non-blocking
        SocketChannel socketChannel = serverSocketChannel.accept();
        Socket socket = socketChannel.socket();
        socketChannel.configureBlocking(false);

        // Register the new SocketChannel with our Selector, indicating
        // we'd like to be notified when there's data waiting to be read
        socketChannel.register(selector, SelectionKey.OP_READ);
        System.out.println("Accepted Connection");
        scl.add(socketChannel);
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        readBuffer.rewind();

        int numRead;
        try {
            numRead = socketChannel.read(readBuffer);

        } catch (IOException e) {

            key.cancel();
            socketChannel.close();
            return;
        }

        if (numRead == -1) {
            key.channel().close();
            key.cancel();
            return;
        }

        worker.processData(this, this.selector, socketChannel, readBuffer.array(), numRead, this.scl);
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        synchronized (pendingData) {
            List<ByteBuffer> queue = pendingData.get(socketChannel);

            while (!queue.isEmpty()) {
                ByteBuffer buf = queue.get(0);
                String str = new String (buf.array(), "UTF-8");
                String[] strarr = str.split(",");
                for (String a : strarr){
                    System.out.println("Writing " + a);
                    socketChannel.write(ByteBuffer.wrap(a.getBytes()));
            
                }
                if (buf.remaining() > 0) {
                    break;
                }
                queue.remove(0);
            }

            if (queue.isEmpty()) {
                key.interestOps(SelectionKey.OP_READ);
            }
        }
    }

    private void initSelector() throws IOException {
        selector = SelectorProvider.provider().openSelector();

        serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        serverChannel.socket().bind(new InetSocketAddress(port));


        serverChannel.register(selector, SelectionKey.OP_ACCEPT);
    }
////////////////////////////////////////////////////////////////////

    public static void main(String[] args) throws InterruptedException{
		if (args.length != 3){
			System.out.println("usage: java ChannelPort IP port-number number-of-nodes");
			System.exit(-1);
		} 
		int portNum = Integer.parseInt(args[1]); 
        int numNode = Integer.parseInt(args[2]); 
        String address = args[0];
        try {
            

            EchoWorker worker = new EchoWorker();
            Thread td = new Thread(worker);
            ChannelPort server = new ChannelPort(InetAddress.getByName(address), portNum, worker);
            Thread td1 = new Thread(server);
            td.start();
            td1.start();
            // td.join();

            System.out.println("MaybeDone?");
            td1.join();
            
        } catch (IOException e) {
            e.printStackTrace();
        
        }
        
    }
}
